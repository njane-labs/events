user_details = {}
events = {}
class User():
    
    def register(self, email, password):
        if user_details.get(email):
            return "email in use, use a different one"
        else:
            user_details[email] = password
            return "registration succesful"

    def login(self, email, password):
        if user_details.get(email):
            if user_details[email] = password:
                return "login succesful"
            else:
                return "wrong password"
        else:
            return "email does not exist. create an account first"

    
class Events():
    
    def create_event(self, owner, event_name):
        if event_name in events.get(owner):
            return "event name in use. use a different one"
        else:
            events[owner] = event_name
            return "event created succesfully."

    def get_events(self, owner):
        if bool(events):
            if events.get(owner):
                return events.get(owner)
            else:
                return []
        else:
            return []

    def delete_event(self, owner, event_name):
        if event_name in events(owner):
            events[owner].remove(event_name)
            return "deletion succesful"
        else:
            return events

    def edit_event(self, owner, event_name, new_name):
        if event_name in events.get(owner):
            events[owner].remove(event_name)
            events[owner].append(new_name)
            return "event has succesfully been updated"
        return "event not found. you can only update an existing event."
